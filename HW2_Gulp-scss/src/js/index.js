const navNode = document.querySelector('.btn--burger');
navNode.addEventListener('click', () => {
    navNode.classList.toggle('btn--close');
    navNode.nextElementSibling.classList.toggle('menu--open');
})