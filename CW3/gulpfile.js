import gulp from 'gulp'
import cleanCSS from 'gulp-clean-css';
import htmlmin from 'gulp-htmlmin';
import concat from 'gulp-concat';
import del from 'del'
import browserSync from 'browser-sync';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';

const sass = gulpSass(dartSass);
const bSync = browserSync.create()

function liveServer() {
    bSync.init({
        server: {
            baseDir: "./dist"
        }
    });
}

export const clean = () => {
    return del('dist')
}

function buildHTML() {
    return gulp.src('src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'))
}

function buildCSS() {
    return gulp.src('src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('dist'))
}

function buildJS(){
    return gulp.src('src/**/*.js')
        .pipe(concat('script.js'))
        .pipe(gulp.dest('dist'))
}

export const build = gulp.series(clean, gulp.parallel(buildCSS, buildHTML, buildJS))

export function start() {
    liveServer()
    gulp.watch(['src/**/*'], build).on('change', () => bSync.reload())
}

export default build
